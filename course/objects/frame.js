// think green, to avoid creating a new object for every single pixel every frame
const reusablePixel = {};

class Frame {
  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.pixelList = new Uint8ClampedArray(width * height);
  }

  updatePixels(list) {
    // console.log(list);
    if (this.width * this.height * 4 !== list.length) {
      throw new Error('Size did not match pixel list');
    }
    this.pixelList = list;
  }

  get(x, y) {
    if (x < 0 || x > this.width) throw Error('x outside image');
    if (y < 0 || y > this.height) throw Error('y outside image');
    const index = (y * this.width + x) * 4;

    reusablePixel.i = index;
    reusablePixel.r = this.pixelList[index];
    reusablePixel.g = this.pixelList[index + 1];
    reusablePixel.b = this.pixelList[index + 2];
    reusablePixel.a = this.pixelList[index + 3];
    return reusablePixel;

    // return {
    //   i: index,
    //   r: this.pixelList[index],
    //   g: this.pixelList[index + 1],
    //   b: this.pixelList[index + 2],
    //   a: this.pixelList[index + 3],
    // };
  }

  set(x, y, pixel) {
    if (x < 0 || x > this.width) throw Error('x outside image');
    if (y < 0 || y > this.height) throw Error('y outside image');
    const index = (y * this.width + x) * 4;
    this.pixelList[index] = pixel.r;
    this.pixelList[index + 1] = pixel.g;
    this.pixelList[index + 2] = pixel.b;
    this.pixelList[index + 3] = pixel.a;
  }
}
