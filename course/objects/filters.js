function copy(inputFrame, outputFrame) {
  for (let y = 0; y < currentFrame.height; y++) {
    for (let x = 0; x < currentFrame.width; x++) {
      const pixel = inputFrame.get(x, y);
      const newPixel = {
        r: pixel.r,
        g: pixel.g,
        b: pixel.b,
        a: pixel.a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

function gray(inputFrame, outputFrame) {
  for (let y = 0; y < currentFrame.height; y++) {
    for (let x = 0; x < currentFrame.width; x++) {
      const { r, g, b, a} = inputFrame.get(x, y);
      const gray = b;
      const newPixel = {
        r: gray,
        g: gray,
        b: gray,
        a: a,
      };
      outputFrame.set(x, y, newPixel);
    }
  }
}

// add your filter functions here, the dropdown on the page will automatically show them
const filters = {
  gray,
  copy,
};
