function makeColor(r, g, b) {
  return 'rgb(' + r + ',' + g + ',' + b + ')';

  // this is preferred in modern javascript:
  return `rgb(${r},${g},${b})`;
}

function updateColor() {
  const red = sliderRed.value;
  const green = sliderGreen.value;
  const blue = sliderBlue.value;

  // could also have used:
  // const red = document.getElementById('sliderRed').value;
  // or:
  // const red = document.querySelector('#sliderRed').value;

  const color = makeColor(red, green, blue);
  colorBox.style.backgroundColor = color;

  const average = (Number(red) + Number(green) + Number(blue)) / 3;
  const gray = makeColor(average, average, average);
  grayBox.style.backgroundColor = gray;
}

// callbacks every time the slider value changes
sliderRed.oninput = updateColor;
sliderGreen.oninput = updateColor;
sliderBlue.oninput = updateColor;

// Bonus task
function setRandomColors() {
  sliderRed.value = Math.random() * 255;
  sliderGreen.value = Math.random() * 255;
  sliderBlue.value = Math.random() * 255;
  updateColor(); // surprising that this is needed. oninput isnt triggered
}

// keep a reference to the timer id, so we can stop it:
let timer = 0;

function toggleRandom() {
  if (randomize.checked) {
    timer = setInterval(setRandomColors, 1500);
  }
  else {
    clearInterval(timer);
  }

  // bonus: disable/enable sliders depending on checkbox:
  Array.from(document.querySelectorAll('input[type=range]')).forEach((slider) => {
    slider.disabled = randomize.checked;
  }); // you might need to read up a bit here :)
}

// call method each time the checkbox is clicked
randomize.onchange = toggleRandom;
