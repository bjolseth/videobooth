/**
 * 1. Add code so that updateColor is updated when slider changes
 * 2. Set grayscale version of color on grayBox
 * Bonus: change to random color every second when checkbox is ticked
 *
 * Hint: oninput is an event property of sliders
 * Hint: slider values are strings, not numbers! Number(myvariable) converts
 * Hint: average of rgb can be used for gray value
 * Hint: checkbox has a 'checked' property
 */
function updateColor() {
  const red = sliderRed.value;
  const green = sliderGreen.value;
  const blue = sliderBlue.value;
  const color = 'rgb(' + red + ',' + green + ',' + blue + ')';
  colorBox.style.backgroundColor = color;
}
