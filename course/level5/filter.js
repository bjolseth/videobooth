/**
 * Called for each video frame
 *
 * @param {stillFrame}: rgba list for the static frame
 * @param {liveFrame}: rgba list for current live video frame
 * @param {output}: rgba list for you to populate
 *
 * If you set alpha = 0 for a pixel, the background will shine through
 *
 * You can use the slider value to adjust a parameter you want to test
 */
function filter(stillFrame, liveFrame, outputFrame, sliderValue = 50) {

  // YOUR CODE HERE

  return outputFrame;
}
