function filter(emptyRoom, live, target, threshold = 70) {
  const empty = emptyRoom;

  let diffRGB = 0;
  for (var i = 0; i < live.length; i += 4) {

    const grayLive = (live[i] + live[i+1] + live[i+2]) / 3;
    const grayEmpty = (empty[i] + empty[i+1] + empty[i+2]) / 3;
    const diff = Math.abs(grayLive - grayEmpty);

    if (diff > threshold) {
      target[i] = live[i];
      target[i + 1] = live[i + 1];
      target[i + 2] = live[i + 2];
      target[i + 3] = 255;
    }
    else {
      target[i] = 0;
      target[i + 1] = 0;
      target[i + 2] = 0;
      target[i + 3] = 0;
    }
  }

  // There's HUGE room for improvement here.
  // Use tracker.js to limit the area to faces?
  // Use contiguous regions to find relevant areas?
  // Filter noise?
  // Smart way to detect changes due to auto-exposure?

  return target;
}
