const webcamCanvas = el('webcam-canvas');
const video = el('webcam');
const emptyRoom = el('empty-room');
const resultCanvas = el('result-canvas');

let tolerance;
let hasEmptySnap = false;

function el(id) {
  return document.getElementById(id);
}

function initWebCam() {
  const video = el('webcam');
  try {
    navigator.mediaDevices.getUserMedia({ audio: false, video: true })
      .then(stream => video.srcObject = stream)
      .catch(error => console.log(error));
  }
  catch (e) {
    alert('No media, no fun');
  }
}

function snapVideo() {
  const ctx = webcamCanvas.getContext('2d');
  ctx.drawImage(video, 0, 0, webcamCanvas.width, webcamCanvas.height);
  return ctx.getImageData(0, 0, webcamCanvas.width, webcamCanvas.height);
}

function startCountdown() {
  let time = 2;
  const countdown = el('countdown');
  countdown.style.display = 'flex';
  countdown.innerText = time;

  const timer = setInterval(() => {
    time -= 1;
    countdown.innerText = time;
    if (time <= 0) {
      snapEmpty();
      clearInterval(timer);
      countdown.style.display = 'none';
    }
  }, 1000);
}

function snapEmpty() {
  const data = snapVideo();
  emptyRoom.getContext('2d').putImageData(data, 0, 0);
  hasEmptySnap = true;
  el('empty-room').style.display = 'block';
  el('info-box').style.display = 'none';
}

function replaceBackground() {
  const resultCtx = resultCanvas.getContext('2d');
  if (!hasEmptySnap) {
    return;
  }
  const liveVideo = snapVideo();

  const { width, height } = emptyRoom;
  const ctx = emptyRoom.getContext('2d');
  const emptyData = ctx.getImageData(0, 0, width, height).data;

  const result = resultCtx.createImageData(width, height);
  const data = filter(emptyData, liveVideo.data, result.data, tolerance);
  result.data = data;
  resultCtx.putImageData(result, 0, 0);
}

function render() {
  replaceBackground();
  window.requestAnimationFrame(render);
}

function setTolerance() {
  tolerance = el('tolerance').value;
  el('tolerance-label').textContent = tolerance;
}

function initGui() {
  el('empty-room').onclick = startCountdown;
  el('info-box').onclick = startCountdown;
  el('tolerance').onchange = setTolerance;
  el('empty-room').style.display = 'none';
  el('countdown').style.display = 'none';
}

function init() {
  initWebCam();
  setTolerance();
  initGui();
  render();
}

window.onload = init;
