/**
 * Called for every video frame. This example is just copying the pixels from input to output
 *
 * @param {inputFrame} contains the RGBA array for all pixels in the current frame
 * @param {outputFrame} similar array where you put your filtered image
 */
function copy(inputFrame, outputFrame) {

  // input frame contains 4 numbers (RGBA) for each pixel
  for (let i = 0; i < inputFrame.length; i += 4) {
    outputFrame[i] = inputFrame[i];
    outputFrame[i + 1] = inputFrame[i + 1];
    outputFrame[i + 2] = inputFrame[i + 2];
    outputFrame[i + 3] = 255; // opaque
  }
}


function grayscale(inputFrame, outputFrame) {
  // your code here
  // hint: gray means r,g and b are equal
}

// Add more filters:
// function invert(inputFrame, outputFrame);
// function brighten(inputFrame, outputFrame);
// function halftone(inputFrame, outputFrame);
// function sepia(inputFrame, outputFrame);

// Formula for sepia:
// red = (0.393 * r + 0.769 * g + 0.189 * b);
// green = (0.349 * r + 0.686 * g+ 0.168 * b);
// blue = (0.272 * r + 0.534 * g + 0.131 * b);


// add your filter functions here, the dropdown on the page will automatically show them
const filters = {
  copy,
  // grayscale
};
