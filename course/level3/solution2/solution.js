
let weights = [0, 0, 0, 0, 1, 0, 0, 0, 0];

// Convolution filters. copied from https://www.html5rocks.com/en/tutorials/canvas/imagefilters/
function convolute(input, output, weights, width, height) {
  var sw = width;
  var sh = height;
  const opaque = 1;

  var src = input;
  var side = Math.round(Math.sqrt(weights.length));
  var halfSide = Math.floor(side/2);
  // pad output by the convolution matrix
  var w = sw;
  var h = sh;

  var dst = output;
  // go through the destination image pixels
  var alphaFac = opaque ? 1 : 0;
  for (var y=0; y<h; y++) {
    for (var x=0; x<w; x++) {
      var sy = y;
      var sx = x;
      var dstOff = (y*w+x)*4;
      // calculate the weighed sum of the source image pixels that
      // fall under the convolution matrix
      var r=0, g=0, b=0, a=0;
      for (var cy=0; cy<side; cy++) {
        for (var cx=0; cx<side; cx++) {
          var scy = sy + cy - halfSide;
          var scx = sx + cx - halfSide;
          if (scy >= 0 && scy < sh && scx >= 0 && scx < sw) {
            var srcOff = (scy*sw+scx)*4;
            var wt = weights[cy*side+cx];
            r += src[srcOff] * wt;
            g += src[srcOff+1] * wt;
            b += src[srcOff+2] * wt;
            a += src[srcOff+3] * wt;
          }
        }
      }
      dst[dstOff] = r;
      dst[dstOff+1] = g;
      dst[dstOff+2] = b;
      dst[dstOff+3] = a + alphaFac*(255-a);
    }
  }
};

function matrix(input, output, options) {
  convolute(input, output, weights, options.width, options.height);
}

function grayscale(input, output) {
  for (let i = 0; i < input.length; i += 4) {
    const avg = (input[i] + input[i+1] + input[i+2]) / 3;
    output[i] = avg;
    output[i + 1] = avg;
    output[i + 2] = avg;
    output[i + 3] = 255;
  }
}


// for edge detection
function sobel(input, output, options) {
  const gray = new Uint8ClampedArray(output.length);
  grayscale(input, gray);

  const { width, height } = options;
  const vertical = new Uint8ClampedArray(output.length);
  const horizontal = new Uint8ClampedArray(output.length);
  convolute(gray, vertical,
    [ -1, 0, 1,
      -2, 0, 2,
      -1, 0, 1 ], width, height);
  convolute(gray, horizontal,
    [ -1, -2, -1,
       0,  0,  0,
       1,  2,  1 ], width, height);

  for (let i = 0; i < output.length; i += 4) {
    // vertical gradient: red
    output[i] = Math.abs(vertical[i]);
    // horizontal gradient: green
    output[i + 1] = Math.abs(horizontal[i]);
    // blue, just for style:
    output[i + 2] = (output[i] + output[i+1]) / 4;
  }
}

function updateMatrix() {
  const inputs = document.querySelectorAll('.matrix input');
  weights = Array.from(inputs).map((input) => {
    return Number(input.value) || 0;
  });
}

document.getElementById('update-matrix').onclick = updateMatrix;

const filters = {
  matrix,
  sobel,
};
