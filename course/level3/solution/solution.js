function limit(value, min, max) {
  return Math.min(Math.max(min, value));
}

function l255(value) {
  return limit(value, 0, 255);
}

// for dat.gui settings panel:
const settings = {
  halfToneThreshold: 128,
  lightColor: [255, 255, 255],
  darkColor: [0, 0, 0],
  brighten: 100,
}

// not part of the task, but always nice to have an easy way to tune algorithms for engineers
function defineSettings() {
  const gui = new dat.GUI();
  const f1 = gui.addFolder('Half tone');
  f1.add(settings, 'halfToneThreshold', 0, 255);
  f1.addColor(settings, 'lightColor');
  f1.addColor(settings, 'darkColor');
  const f2 = gui.addFolder('Brighten');
  f2.add(settings, 'brighten', 0, 255);
};

function copy(input, output) {
  for (let i = 0; i < input.length; i += 4) {
    output[i] = input[i];
    output[i + 1] = input[i + 1];
    output[i + 2] = input[i + 2];
    output[i + 3] = 255;
  }
}

function invert(input, output) {
  for (let i = 0; i < input.length; i += 4) {
    output[i] = 255 - input[i];
    output[i + 1] = 255 - input[i + 1];
    output[i + 2] = 255 - input[i + 2];
    output[i + 3] = 255;
  }
}

function grayscale(input, output) {
  for (let i = 0; i < input.length; i += 4) {
    const avg = (input[i] + input[i+1] + input[i+2]) / 3;
    output[i] = avg;
    output[i + 1] = avg;
    output[i + 2] = avg;
    output[i + 3] = 255;
  }
}

function halftone(input, output) {
  const threshold = settings.halfToneThreshold;
  for (let i = 0; i < input.length; i += 4) {
    const avg = (input[i] + input[i+1] + input[i+2]) / 2;
    const [r, g, b] = avg > threshold ? settings.lightColor : settings.darkColor;
    output[i] = r;
    output[i + 1] = g;
    output[i + 2] = b;
    output[i + 3] = 255;
  }
}

function brighten(input, output, options = {}) {
  const increase = settings.brighten;
  for (let i = 0; i < input.length; i += 4) {
    output[i] = l255(input[i] + increase);
    output[i + 1] = l255(input[i + 1] + increase);
    output[i + 2] = l255(input[i + 2] + increase);
    output[i + 3] = 255;
  }
}

function onlyRed(input, output, options = {}) {
  const upper = 130;
  const lower = 100;
  for (let i = 0; i < input.length; i += 4) {
    // if (input[i] > upper && input[i+1] < lower && input[i+2] < lower) {
    if (input[i] > upper && input[i+1] < lower && input[i+2] < lower) {
      output[i] = input[i]
      output[i + 1] = 0;
      output[i + 2] = 0;
    }
    else {
      const avg = (input[i] + input[i + 1] + input[i + 2]) / 3
      output[i] = avg;
      output[i + 1] = avg;
      output[i + 2] = avg;
    }
    output[i + 3] = 255;
  }
}

function sepia(input, output) {
  // https://www.dyclassroom.com/image-processing-project/how-to-convert-a-color-image-into-sepia-image
  for (let i = 0; i < input.length; i += 4) {
    const r = input[i], g = input[i+1], b = input[i+2];
    output[i] = l255(0.393 * r + 0.769 * g + 0.189 * b);
    output[i + 1] = l255(0.349 * r + 0.686 * g+ 0.168 * b);
    output[i + 2] = l255(0.272 * r + 0.534 * g + 0.131 * b);
    output[i + 3] = 255;
  }
}

const filters = {
  copy,
  invert,
  brighten,
  sepia,
  onlyRed,
  halftone,
  grayscale,
  invert,
};
