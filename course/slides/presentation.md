
# JavaScript: Hack Your Face

Tore Bjølseth, tbjolset@cisco.com

# JavaScript Course NTNU 2019

## Content

* A quick intro / crash course to JavaScript
* Interactive sessions
* Theme: image processing
* More focus on fun and inspiration than theory

Warning: The code we show in this course is not safe for work.

## Content

- Level 1: Variables, functions
- Level 2: Callbacks, timers
- Level 3: Arrays, bitmaps
- Level 4: Canvas, tracking.js
- Boss level: Objects
- Next game: Hackathon

## Challenges

- Browsers: Recommend Chrome or Firefox
- Download and unzip code and slides: cs.co/hackmyface
- Double click slides/index.html to view presentation
- At the end of each level is a code challenge
- Do them in groups of 2 or 3
- A proposed solution is in the solution/ folder
- Read the hints in the code
- Give it a good try and ask for help before you look at the solution
- You can follow the levels or go with your own flow

# Level 1

## Language features

* Core web technology together with HTML and CSS
* Especially popular for dynamic web pages
* Dynamically typed
* High-level scripting language, not compiled
* First class functions
* Single threaded, event driven

## Short history

* Created by Brendan Eich at Netscape in 1995
* Name inspired by Java, but not code
* Ajax, Gmail 2004
* Browser wars: ~2000-2010
* Server side JavaScript in Node since 2009
* (For more on Node, join the Cisco Hackathon cs.co/hackcisco2019!)

## Today

Oh my God is that a web page?

* WebGL for high performance, powerful graphics
* dat.gui for tuning parameters

Demo: https://paveldogreat.github.io/WebGL-Fluid-Simulation/ Fluid Simulation


## A Simple page

```
<html>
<body>
  <button id="start" onclick="start()">Ready player 1</button>
</body>
<script>
  function start() {
    alert('Play!');
  }
</script>
</html>
```

## Variables

- **let** can be changed:

```
let coins = 0;
coins = 22; // ok
```

- **const** cannot be changed:

```
const pi = 3.14;
pi = 9.81; // error
```

- Old school

```
var dont = 'use me no more';
```

## Types

```
const name = 'mario'; // string
const lives = 3;      // number
const isBig = true;   // bool
const player = {
  name: name,
  lives: lives,
  isBig: isBig,
};                    // object
function run() {...}  // function
const boss = null;    // null
const dragon;         // undefined
```

* You can change types implicitly

```
let brother = 'luigi';
brother = 42;
```

## Functions

- Functions look similar to Java, Python etc

```
function pythagoras(a, b) {
  const c = Math.sqrt(a*a + b*b);
  console.log('c=', c);
}
pythagoras(3, 4); // prints 5
```


## Functions

- Functions can be assigned to variables

```
jumpButton.onclick = function jump(height) {
  console.log('jump', height);
};
```

- The same, but with fat arrow syntax:

```
jumpButton.onclick = (height) => console.log('jump', height);
```

## if, else

* Flow controls is similar to other languages

```
let marioIsBig = true;
let lives = 3;
// ...
while (lives >= 0) {
  if (marioIsBig) {
    marioIsBig = false;
  }
  else {
    lives--;
  }
}
```

## Updating a web page

```
<html>
<style>
  button {
    background-color: blue;
    color: white;
  }
</style>
<body>
  <button id="start">Ready player 1</button>
</body>
<script>
  const button = document.getElementById('start');
  button.style.backgroundColor = 'orange';
</script>
</html>
```

## Colours

* Each pixel has four components:

* The amount of red, blue and green (0-255)

* The amount of transparency (0-255)

* White: all colours are 255

* Black: all colours are 0

* What defines gray?


## Ready Player 1

* Open level1/index.html in your editor and browser

* Let's make the sliders change the color box

* We will go through this task together on screen

Demo: ../level1 Show exercise

# Level 2

## Callbacks

* In JavaScript, functions are first class citizens

* They can be passed as arguments and stored as other variables

* Callbacks are typically useful when dealing with events and "slow" operations:

* JavaScript is a single threaded language

```
function showWeather(temperature) {
  alert("Tomorrow's temperature:" + temperature);
}
network.request('yr.no/getForecast', showWeather); // Note: no parenthesis
```

## Timers

* Single timer

```
setTimeout(() => changeTitle('Super Mario'), 2000);
function changeTitle(newTitle) {
  document.title = title;
}
```

* Repeating timer

```
let coins = 0;
const timer = setInterval(collectCoin, 300);
function collectCoin() {
  console.log(coins++);
  if (coins > 10) {
    clearInterval(timer); // stops the timer
  }
}
```

## Ready player 1

* Open level2/index.js in your editor and browser

* Add a callback to the sliders so they update the color when changed

* Set a gray version of the color too

* Hint: Take the average for red, green and blue, and use this for all colors

* Bonus level: set a new random color every second

Demo: ../level2 Show exercise


## Inspiring example 2

* MazeMap
* Create building maps with machine learning from CAD building models
* Based just across Digs
* Pure JavaScript / WebGL
* Works seamlessly on mobile
* Access to mobile GPS API

Demo: http://use.mazemap.com MazeMap NTNU

# Level 3

## Arrays and loops

- Create an array

```
const myPixels = [123, 345, 225, 12, 345];
```

- Traverse an array

```
for (let i = 0; i < myPixels.length; i++) {
  console.log(myPixels[i]);
}
// Or:
for (let value of myPixels) {
  console.log(value);
}
// Or:
myPixels.forEach((value) => console.log(value));
```

## User media

* Get access to the web camera.
* Used for WebRTC (web video calls)
* Requires https

```
<video id="myVideo"></video>
<script>
  const video = document.querySelector('video');
  navigator.mediaDevices
    .getUserMedia({ video: true, audio: true })
    .then((stream) => { video.srcObject = stream }); // "promise"
</script>
```

## Bitmap array

* Four values per pixel: red, green, blue, alpha
* Flat list
* Left to right, top to bottom
* Gray: red, green and blue are equal

Four pixel bitmap: <img src="assets/pixels.png" height="50px" />

```
const myBitmap = [
  255, 0, 0, 255,
  0, 255, 0, 255,
  0, 0, 255, 255,
  128, 128, 128, 255,
];
```

## Ready player 1

* Open level3/filter.js
* Create a grayscale filter

Easy peasy? Bonus levels:

* Invert colors
* Half tone (each pixel is either black or white)
* Brighten the image
* Add dat.gui to adjust settings

Demo: ../level3 Show exercise

## Self study: Convolution filters

* Simple matrix to alter the pixel based on nearby pixels

```
// Example: sharpen filter
const matrix = [
  0, -1, 0,
  -1, 5, -1,
  0, -1, 0,
];
const newImage = matrixMultiply(allImagePixels, matrix);
```

* https://www.html5rocks.com/en/tutorials/canvas/imagefilters/

Demo: ../level3/solution2 Demo

# Level 4

## tracking.js

* JavaScript libraries are often free and easy to use
* Lets you do something you wouldn't be able to do yourself
* Or saves you time
* tracking.js helps you track face, eyes and mouth


## Canvas

Use to draw graphics on screen, such as circles, rectangles and images.

```
<canvas id="game" width="300" height="300"></canvas>
<script>
  const c = document.getElementById("game");
  const ctx = c.getContext("2d");
  ctx.moveTo(0, 0);
  ctx.lineTo(200, 100);
  ctx.strokeStyle = 'red';
  ctx.stroke();
</script>
```

## Ready player 1

* Open level4/filter.js
* Find some images to put on your face. Pig nose, funny glasses, be creative
* Draw and scale the images on your face(s)

Bonus levels

* Stabilise the objects
* Switch faces on two persons

Demo: ../level4 Show exercise


# Level 5 (Boss)

## Objects

* Use {} to construct
* Provides key/value pairs
* Can be nested
* Access properties with dot
* Same same as JSON (JavaScript Object Notation)

```
const enemy23 = {
  type: 'duck',
  position: {
    x: 22,
    y: 147,
  },
  alive: true,
};
enemy23.position.x += 2;
```

## Objects

* Shortcuts

```
const x = 10, y = 20;
const pos = { x: x, y: y };
const pos2 = { x, y }; // shortcut, exactly the same as above
```

* Object 'destructuring'

```
function updatePos(pos) {
  const x = pos.x;
  const y = pos.y;
  console.log(x, y);
}
function updatePos2({ x, y }) {
  console.log(x, y); // shortcut, exactly the same as above
}
```



## Exception handling

* JavaScript can throw any type as an exception

```
function setScore(score) {
  if (typeof score !== 'number') {
    throw new Error('setScore requires a number');
  }
  // ...
}
```

* Usage

```
try {
  setScore('luigi');
  playFanfare(); // won't be called
}
catch (e) {
  console.error('Game over', e);
}
```

## Classes

* With EcmaScript 6, JavaScript finally has class support

```
class Level {
  constructor(points) {
    this.points = points;
  }
  increasePoints(increment) {
    this.points += incremenet;
  }
}
const level3 = new Level(120);
level3.increasePoints(30);
console.log(level3.points); // 150
```

## Ready player 1

* Open level5/filter.js
* Offices are messy and dull
* Replace the background with something better
* Hint: make the pixels that haven't changed transparent (alpha)

Bonus level:

* Use whathever means to get better results? Filters? Tracking info?

Demo: ../level5 Show exercise

## We skipped a lot

* Type casting
* Scoping, closures
* HTML elements, the Document Object Model (DOM)
* Promises, async
* Built-ins: String, Date, Array, Math, ...
* Making HTTP requests
* SVG, WebGL, Web Audio
* Transpilers
* Node.js (server side JavaScript)
* Libraries: jQuery, React, Angular, BootStrap, ...


## Thank you!

* Remember to join Hackathon
* cs.co/hackcisco2019
* 23-27 Oct
* Max 50 participants
* Prize: Trip to SXSW in Austin Texas, everything included
