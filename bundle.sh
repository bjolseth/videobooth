#!/usr/bin/env bash

# Make a bundle that course students can download. Remove solutions

# Remove solutions and make zip to download for students
rm -Rf hackmyface
rm course/hackmyface.zip
cp -r course hackmyface
zip -r hackmyface.zip hackmyface/
mv hackmyface.zip course/
rm -Rf hackmyface/

echo To push:
echo scp hackmyface.zip bjolseth@kyberheimen.com:~/kyberheimen/hackmyface
